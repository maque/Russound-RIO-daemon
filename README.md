# Russound-RIO-daemon
The purpose of riod.py to permanelty run as a daemon on a unix hosts, e.g. raspberry and provide the status of a Russound device MCA-C3, MCA-C5 or MCA-88.
It always able to provide the current configuration via built in Web Service and could send update to specific hosts via TCP, UPD and MQTT to provide the status of a Russound device MCA-C3, MCA-C5 or MCA-88.
It has been only tested with python3. Location of riod.ini could be either script dir, /etc or /usr/local/etc
The ini file is mandatory, to configure russound connection, Radio channels, as well as any outbound udp, tcp or mqtt connections  

There are 2 different ways to send commands to russound:  

### __http__   
Example: http://127.0.0.1:8080/cmd?action=on&zone=1&source=2 
Parameters, like ports or TLS, have to be defined in ini-file in section __[Webserver]__.  
For testing purposed with a standard Webbrowser just enter the url. To use curl: curl "http://127.0.0.1:8080/cmd?action=on&zone=1&source=2"
  

### __mqtt__   
Parameters, like ports, TLS or topics, have to be defined in ini-file in section __[MQTT]__. Topics are case-sensitive  
The root topics can be also defined in the ini-file. Sub-Topics are  
- /Cmd - Send Commands to the Script<br>
- /Ack - Acknoledge /Cmd<br>
- /Get - Retrieve Information from Russound Device<br>
- /Data - Response to a /Get request<br>
- /Set - Change some settings on the running proccess like debugLevel=0,1,2  


### action:  
__On__ - zone power on : zone, source  
__play__ - play a source and specific channel : zone, source, channel  
__Off__ - zone power off : zone  
__source__ - set source of zone : zone, source  
__volume__ - set volume of zone, a number with a leading '+' or '-' changes the volume relatively: zone, volume  
__volup__ - Increase the volume by 1 step: zone  
__voldown__ - Decrease the volume by 1  step: zone  
__bass__ - set bass of zone : zone, bass  
__treble__ - set treble of zone : zone, treble  
__balance__ - set bass of zone : zone, balance  
__turnOnVolume__ - set turnOnVolume to volume for zone  

### Parameter:
__zone__: Zone number e.g. 1 or 1,4,5 etc...  
__controller__: Controller number e.g. 1 ( default is 1)  
__volume__: Volume for announcements and for fade-in (1..50)  
__source__: set zone to source number  
__bass__: bass value to be send -10 to 10  
__treble__: treble to be send -10 to 10  
__balance__: Balance -10 to 10  
__channel__: Radio Channel, defined in riod.ini, to be changed to

### Enable ssl:
The service supports ssl connections. It has to be enabled in the ini file. Private key, Cert and CA file have to be copied in one bundle file, like "cat keyfile certfile cafile > bundle.crt"  

### Autostart
The easiest way to run the script at startup for a raspberry pi would be 
1. cp systemd/riod.service /lib/systemd/system/riod.service
2. sudo systemctl daemon-reload
3. sudo systemctl enable riod.service
4. sudo systemctl start riod.service

On other systems, the steps would be similar, but may not be exactly the same, e.g. settings in systemd/riod.service may have to be adapted accordingly
